//Soal No. 1 (Range) 

var hasil = []; 

function range(startNum, finishNum) {
    if (startNum==null || finishNum==null) {
        return -1;
    } else if (startNum<=finishNum) {
        hasil = [];
        for (i = startNum; i <=finishNum; i++) {
            hasil.push(i);
        }   
    } else {
        hasil = [];
        for (i = startNum; i >=finishNum; i--) {
            hasil.push(i);
        }         
    }
    return hasil;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


// Soal No. 2 (Range with Step)

var hasil = [];
function rangeWithStep(startNum, finishNum, step) {
    if (startNum==null || finishNum==null) {
        return -1;
    } else if (startNum<=finishNum) {
        hasil = [];
        for (i = startNum; i <=finishNum; i += step) {
            hasil.push(i);      
        }   
    } else {
        hasil = [];
        for (i = startNum; i >=finishNum; i -= step) {
            hasil.push(i);
        }         
    }
    return hasil;
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


//Soal No. 3 (Sum of Range)

var hasil = 0;
function sum(startNum, finishNum, step) {
    if (finishNum==null) {
        finishNum=1;
    }
    if (step==null) {
        step=1;
    }

    if (startNum<=finishNum) {
        hasil = 0;
        for (i = startNum; i <=finishNum; i += step) {
            hasil=hasil+i;      
        }   
    } else {
        hasil = 0;
        for (i = startNum; i >=finishNum; i -= step) {
            hasil=hasil+i; 
        }         
    }
    return hasil;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)

function dataHandling(inputData) {
    var hasil='';
    for(var i=0; i< inputData.length; i++) {
        var x= 'Nomor ID:  '+inputData[i][0]+'\nNama Lengkap:  '+inputData[i][1]+'\nTTL: '+inputData[i][2]+' '+inputData[i][3]+'\nHobi: '+inputData[i][4]+'\n';
        hasil = hasil + '\n' + x;
    }
    return hasil;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log(dataHandling(input))


// Soal No. 5 (Balik Kata)

function balikKata(kata) {
    var splitKata = kata.split('');
    var reverseArray = splitKata.reverse();
    var hasil = reverseArray.join('');

    return hasil;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// Soal No. 6 (Metode Array)

function dataHandling2(data) {
    data.splice(1, 2)
    data.splice(2, 1)
    data.splice(1, 0, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    data.splice(4, 0, "Pria", "SMA Internasional Metro")

    console.log(data);

    var tanggal = data[3]; 
    tanggal = tanggal.split("/")
    var tanggalx = tanggal.join("-")
    var bulan = tanggal[1]

    switch(bulan) {
    case '01': bulan = "Januari"; break;
    case '02': bulan = "Februari"; break;
    case '03': bulan = "Maret"; break;
    case '04': bulan = "April"; break;
    case '05': bulan = "Mei"; break;
    case '06': bulan = "Juni"; break;
    case '07': bulan = "Juli"; break;
    case '08': bulan = "Agustus"; break;
    case '09': bulan = "September"; break;
    case '10': bulan = "Oktober"; break;
    case '11': bulan = "November"; break;
    case '12': bulan = "Desember"; break;
   }

    console.log(bulan);

    var urut = tanggal.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(urut)

    console.log(tanggalx)

    var nama = data[1]
    var nama1 = nama.slice(0,15) 
    console.log(nama1) //[1, 2, 3]

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
