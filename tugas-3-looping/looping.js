//No. 1 Looping While

var awal = 1;
var akhir = 20;

console.log('LOOPING PERTAMA');
while( awal <= akhir ){
    if(awal%2==0) {
        console.log(awal+' - I love coding');
    }
    awal++;
}

var awal = 1;
var akhir = 20;
console.log('\nLOOPING KEDUA');
while( akhir >= awal ){
    if(akhir%2==0) {
        console.log(akhir+' - I will become a mobile developer');
    }
    akhir--;
}


//No. 2 Looping menggunakan for

console.log('\nLOOPING MENGGUNAKAN FOR');
var i;
for (i=1;i<=20;i++) {
    if(i%2==0) {
        console.log(i+' - Berkualitas');
    } else {
        if(i%3==0) {
            console.log(i+' - I Love Coding');
        } else {
            console.log(i+' - Santai');
        }
    }
}


//No. 3 Membuat Persegi Panjang #

console.log('\nMEMBUAT PERSEGI PANJANG');
var x='';
for (var i=1;i<=4;i++){
   for (var j=1;j<=8;j++){
      x+='#';
   }
   x+='\n';
}
console.log(x);


//No. 4 Membuat Tangga 

console.log('\nMEMBUAT TANGGA');
var x='';
for (var i = 1; i <= 7; i++){
   for (var j = 1; j <= i;j++){
      x+='#';
   }
   x+='\n';
}
console.log(x);


//No. 5 Membuat Papan Catur 

console.log('\nMEMBUAT PAPAN CATUR');

var x = '';

 for (var i = 0; i < 8; i++) {
     if(i>0) x += '\n';
     for (var j = 0; j < 8; j++) {
         if ((i + j) % 2 == 0){
             x += ' ';
         } else {
             x += '#';
         }
     }
 }

 console.log(x);